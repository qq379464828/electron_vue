## electron打包实例

**步骤一：初始化Vue.js项目**

首先，我们需要创建一个新的Vue.js项目。在命令行中执行以下命令：

```bash
vue create my-electron-app  
cd my-electron-app
```

**步骤二：安装Electron和electron-builder**

然后，我们需要安装Electron和electron-builder。在项目的根目录下执行以下命令：

```bash
npm install electron --save-dev  
npm install electron-builder --save-dev
```

**步骤三：配置package.json**

接下来，我们需要配置`package.json`文件，添加Electron和electron-builder的相关配置。在`package.json`文件中添加以下内容：

```json
{  
  "name": "my-electron-app",  
  "version": "1.0.0",  
  "description": "My Electron application description",  
  "main": "background.js", // Electron应用的入口文件  
  "scripts": {  
    "start": "electron .", // 运行Electron应用的命令  
    "dist": "electron-builder" // 打包Electron应用的命令  
  },  
  "build": {  
    "appId": "com.example.yourapp", // 应用的唯一标识符  
    "mac": {  
      "category": "your.app.category.type" // 应用在Mac OS X上的类别  
    },  
    "linux": {  
      "target": [  
        "AppImage",  
        "deb"  
      ],  
      "category": "Utility" // 应用在Linux上的类别  
    },  
    "win": {  
      "target": [  
        "nsis"  
      ]  
    }  
  },  
  "devDependencies": {  
    "electron": "^x.y.z", // 替换为实际的Electron版本号  
    "electron-builder": "^x.y.z" // 替换为实际的electron-builder版本号  
  }  
}
```

**步骤四：创建Electron的入口文件**

在项目的根目录下创建一个名为`background.js`的文件，作为Electron应用的入口文件。这个文件将负责创建主窗口，并加载Vue.js应用。示例代码如下：

```javascript
const { app, BrowserWindow } = require('electron')  
const path = require('path')  
  
function createWindow () {  
  const mainWindow = new BrowserWindow({  
    width: 800,  
    height: 600,  
    webPreferences: {  
      nodeIntegration: true,  
      contextIsolation: false,  
      preload: path.join(__dirname, 'preload.js') // 预加载脚本（可选）  
    }  
  })  
  
  mainWindow.loadFile('index.html') // 加载Vue.js应用的入口文件  
}  
  
app.whenReady().then(createWindow)  
  
app.on('window-all-closed', function () {  
  if (process.platform !== 'darwin') app.quit()  
})  
  
app.on('activate', function () {  
  if (BrowserWindow.getAllWindows().length === 0) createWindow()  
})
```

**步骤五：创建预加载脚本（可选）**

如果需要，你可以创建一个预加载脚本（`preload.js`），用于在渲染进程和主进程之间安全地暴露Node.js功能。

**步骤六：构建和打包**

最后，我们可以使用npm脚本来构建和打包我们的Electron应用。在命令行中执行以下命令：

```bash
npm run build # 构建Vue.js应用  
npm run dist # 打包Electron应用
```

执行完上述命令后，electron-builder将开始打包你的Electron应用，并生成可在目标平台上运行的安装包。

请注意，这只是一个基本的示例，实际的Electron和Vue.js项目可能需要更多的配置和代码。此外，你可能还需要根据你的具体需求调整`package.json`中的配置选项。建议查阅Electron和electron-builder的官方文档以获取更详细的信息和配置选项。