## electron_vue

### vue项目初始化

```
cnpm create vue@latest
```

```
cnpm create vue@latest
Need to install the following packages:
create-vue@3.10.2
Ok to proceed? (y) 

Vue.js - The Progressive JavaScript Framework

✔ 请输入项目名称： … electron_vue
✔ 是否使用 TypeScript 语法？ … 否 / 是
✔ 是否启用 JSX 支持？ … 否 / 是
✔ 是否引入 Vue Router 进行单页面应用开发？ … 否 / 是
✔ 是否引入 Pinia 用于状态管理？ … 否 / 是
✔ 是否引入 Vitest 用于单元测试？ … 否 / 是
✔ 是否要引入一款端到端（End to End）测试工具？ › 不需要
✔ 是否引入 ESLint 用于代码质量检测？ … 否 / 是
✔ 是否引入 Vue DevTools 7 扩展用于调试? (试验阶段) … 否 / 是

正在初始化项目 /Users/wby/Desktop/未命名文件夹/electron_vue...

项目初始化完成，可执行以下命令：

  cd electron_vue
  npm install
  npm run dev
```

### electron

##### 安装electron

```
cnpm install electron --save-dev
```

---

###### 参考electron-quick-start

> 新建一个文件夹然后执行

```
git clone https://github.com/electron/electron-quick-start
```

---

##### 初始化electron

在根目录创建 `electron` 文件夹

```
复制electron-quick-start中的 main.js、index.html 到electron文件夹
```

main.js中修改

```
mainWindow.loadFile('index.html')
-->
mainWindow.loadFile(path.resolve(__dirname, "index.html"))
// mainWindow.loadFile('./electron/index.html')
// mainWindow.loadURL("http://www.baidu.com/");
```

修改 `package.json` 文件，指定 `electron/main.js` 为 `Electron` 的入口文件，并添加 `electron:dev` 命令以开发模式运行 `Electron`：

```json
{
  "type": "commonjs",
  "main": "electron/main.js",
  "scripts": {
    "electron:dev": "electron ."
  }
}
```

##### 测试效果

```
npm run electron:dev
```













# Typora Template:

源代码模式	Command /



标题	Command 1 - 6 

段落	Command 0



**粗体**	Command B

*斜体	Command I

<u>下划线</u>	Command U

`代码块` 	Control `



~~删除线~~	Control shift `

<!--注释--> Control -

[超链接]()	Command K



1. 有序列表	Command Alt O

- 无序列表	Command Alt U

- [x] 任务列表	Command Alt X



[链接引用]: baidu.com	"Command Alt L"

[^脚注]: Command Alt R

> 引用	Command Alt Q



```
代码块 Command Alt C
```

| 表格 Command Alt T |      |      |      |
| :----------------- | ---- | ---- | ---- |



分割线	Command Alt -

---

